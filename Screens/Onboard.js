/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Button, Dimensions, TouchableOpacity, ImageBackground } from 'react-native';
import { Pages } from 'react-native-pages';

var { height, width } = Dimensions.get('window');

export default class Onboard extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (

            <View style={styles.container}>
                <ImageBackground source={require('../image/background.png')} style={{ width: '100%', height: '100%' }}>
                    <View>
                        <View style={{ alignItems: "flex-end" }}>
                            <Text style={{ color: "#969899", fontSize: 15, padding: 30, }}>Skip</Text>
                        </View>
                    </View>
                    <View style={styles.btop}>
                        <Text style={{ fontSize: 15, padding: 30, alignItems: "center", justifyContent: "center" }}> Hi there! Welcome to</Text>
                        <Image source={require('../image/TTD.png')} style={{ margin: 10 }} />
                        <Pages indicatorColor="#5a95ff" style={{ width: width, height: 100, }}>
                            <Text style={{ color: "#4f4f4f", textAlign: "center", margin: 48, fontSize: 15, padding: 10, indicatorColor: "#ffb6ff" }}>Very simple Things To-Do List. Help you to manage yourdaily life, without any hassle! </Text>
                            <Text style={{ color: "#4f4f4f", textAlign: "center", margin: 48, fontSize: 15, padding: 10, indicatorColor: "#ffb6ff" }}>Very simple Things To-Do List. Help you to manage yourdaily life, without any hassle! </Text>
                            <Text style={{ color: "#4f4f4f", textAlign: "center", margin: 48, fontSize: 15, padding: 10, indicatorColor: "#ffb6ff" }}>Very simple Things To-Do List. Help you to manage yourdaily life, without any hassle! </Text>
                        </Pages>
                    </View>

                    <View style={styles.button}>

                        <View style={{ marginBottom: 30, width: 300, height: 50, borderRadius: 5, backgroundColor: "#3b5798", flexDirection: "row" }}>
                            <TouchableOpacity onPress={() => { navigate('Monthly_view') }} style={{
                                height: 50, width: 300, borderRadius: 5, flexDirection: "row", alignItems:
                                    "center",

                            }}>
                                <Image source={require('../image/icon_face.png')} style={{ height: 50, width: 50, borderRadius: 5 }} />
                                <Text style={{ color: "#ffffff", flex: 1, marginLeft: 25 }}>FACEBOOK</Text>
                            </TouchableOpacity>

                        </View>

                        <View style={{ width: 300, height: 50, borderRadius: 5, backgroundColor: "#59dcff", flexDirection: "row" }}>
                            <TouchableOpacity
                                onPress={() => { navigate('Monthly_view') }}
                                style={{ height: 50, width: 300, borderRadius: 5, flexDirection: "row", alignItems: "center" }}>
                                <Image source={require('../image/icon_tw.png')} style={{ height: 50, width: 50, borderRadius: 5 }} />
                                <Text style={{ color: "#ffffff", flex: 1, marginLeft: 25 }}> TWITTER</Text>
                            </TouchableOpacity>

                        </View>



                    </View>
                </ImageBackground>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#F5FCFF',
        flexDirection: 'column',
    },
    atop: {
        flex: 1,

    },
    btop: {
        width: width,
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',

    },
    button: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',


    },

});
