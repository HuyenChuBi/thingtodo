import React from 'react';
import { Text, View, Image, TouchableOpacity, FlatList, StyleSheet} from 'react-native';
import  { CardTask } from './CardTask';
import DateTimePicker from 'react-native-modal-datetime-picker';

import { height, width } from '../App'

export default class DayView extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            isDateTimePickerVisible: false, 
            date: 15,
            month: 'February',
            year: 2018
        }
    }
    
    data = [
        {task: 'Meet Lorence', time: '12 pm'},
        {task: 'Meet Lorence', time: '12 pm'},
        {task: 'Meet Lorence', time: '12 pm'},
        {task: 'Meet Lorence', time: '12 pm'},
        {task: 'Meet Lorence', time: '12 pm'},
        {task: 'Meet Lorence', time: '12 pm'},
        {task: 'Meet Lorence', time: '12 pm'},
        {task: 'Meet Lorence', time: '12 pm'},
    ]

    month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    _renderItem = ({item}) => <CardTask task = {item.task} time = { item.time }/>

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        this.setState({
            date: date.getDate(),
            month: this.month[date.getMonth()],
            year: 1900 + date.getYear()
        })
        this._hideDateTimePicker();
    };

    render() {
        return(
            <View style = { styles.container}>
                <View style = { styles.content_1 }>
                    <Text style = {{ fontSize: 30, color: '#000000' }}>{`${this.state.date} ${this.state.month} ${this.state.year}`} </Text>
                    <TouchableOpacity style = {{}} onPress = { this._showDateTimePicker }>
                        <View style = {{ height: 50, width: 50, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center' }}>
                            <Image source= {require('../image/day_view/Bitmap.png')}/>
                        </View>
                    </TouchableOpacity>
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                    />
                </View>
                <View style = { styles.content_2 }>
                    <FlatList   data = { this.data }
                                renderItem = { this._renderItem } 
                                showsVerticalScrollIndicator = { false }          
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 350,
        justifyContent: 'center',
        marginHorizontal: 35,
    },
    content_1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
        marginBottom: 15
    },
    content_2: {
        flex: 6,
    },
    imageCalendar: {
        height: 30,
        width: 30
    }
})