import React from 'react';
import { Text, View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import Swipeout from 'react-native-swipeout';
import { height, width } from '../App'

 
var swipeoutBtns = [
    {
        component: (
        <View style={{
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
                backgroundColor: '#FE5757',
                height: 48,
                width: width/4.5,
            }}
        >
            <Image source= {require('../image/day_view/Bitmap.png')}/>
            <Text style = {{color: '#ffffff', marginLeft: 5, letterSpacing: 1}}> Delete </Text>
        </View>
        ),
        backgroundColor: '#FE5757',

    }
]

export class CardTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return(
            <Swipeout   
                right = { swipeoutBtns }
                backgroundColor = { '#FFFFFF' }
                style = {{ marginTop: 10, borderRadius: 3, height: 48, justifyContent: 'center' }}
                buttonWidth = { width/4 }
                close = { true }
            >
                <View style = { styles.container }>
                    <View style = {{ flexDirection: 'row'}}>
                        { true && <Image source = {require('../image/day_view/ok.png')} style = {{ marginRight: 5}}/> }
                        <Text style = {{color: '#000000', fontSize: 16.5, textDecorationLine: true ? 'line-through': 'none'}}> {this.props.task} </Text>
                    </View>
                    <Text style = {{color: '#000000', fontSize: 16.5}}>{this.props.time}   </Text>
                </View>
            </Swipeout>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 48,
        width: width/4.5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
})