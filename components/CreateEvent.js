import React from 'react';
import {Modal, Text, TouchableHighlight, View, TextInput, StyleSheet} from 'react-native';

const color = require('../App');

export class CreateEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
        };
    }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
            <Modal
                animationType = "fade"
                transparent= { true }
                visible={this.state.modalVisible}
                onRequestClose = { () => console.log('close')}
            >
                <View style = {styles.container}>
                    <View style = { styles.contentInput }>
                        <View style = {{alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row'}} >
                            <Text style = {{fontSize: 28, marginRight: 10, color: '#000000'}}>Create New Tasks </Text>
                            <View style = {{ height: 0.5, flexGrow: 1, borderWidth: 0.5, borderColor: color.gray, marginRight: -10}}/>
                        </View>
                        
                        <View>
                            <Text style = {{color: '#000000', fontSize: 16, marginBottom: -5}}> Topic </Text>
                            <TextInput
                                style = {{fontSize: 16,}}
                                placeholder = { 'Write Topic' }
                                placeholderTextColor = {'#C0C0C0'}
                                underlineColorAndroid = {'#C0C0C0'}
                            >
                            </TextInput>
                        </View>

                        <View>
                            <Text style = {{color: '#000000', fontSize: 16, marginBottom: -5}}> Description</Text>
                            <TextInput
                                style = {{fontSize: 16}}
                                placeholder = { 'Write Description' }
                                placeholderTextColor = {'#C0C0C0'}
                                underlineColorAndroid = {'#C0C0C0'}
                            >
                            </TextInput>
                        </View>

                        <View style = {{ flexDirection: 'row'}}>
                            <TextInput
                                style = {{fontSize: 16, marginRight: 10, flexGrow: 1}}
                                placeholder = { '6.pm' }
                                placeholderTextColor = {'#C0C0C0'}
                                underlineColorAndroid = {'#C0C0C0'}
                            >
                            </TextInput>

                            <TextInput
                                style = {{fontSize: 16, marginRight: 10, flexGrow: 1}}
                                placeholder = { '04' }
                                placeholderTextColor = {'#C0C0C0'}
                                underlineColorAndroid = {'#C0C0C0'}
                            >
                            </TextInput>

                            <TextInput
                                style = {{fontSize: 16, marginRight: 10, flexGrow: 1}}
                                placeholder = { 'Feb' }
                                placeholderTextColor = {'#C0C0C0'}
                                underlineColorAndroid = {'#C0C0C0'}
                            >
                            </TextInput>

                            <TextInput
                                style = {{fontSize: 16, flexGrow: 1}}
                                placeholder = { '2018' }
                                placeholderTextColor = {'#C0C0C0'}
                                underlineColorAndroid = {'#C0C0C0'}
                            >
                            </TextInput>
                        </View>

                        <View>
                            <Text style = {{color: '#000000', fontSize: 16, marginBottom: -5}}> Notification </Text>
                            <TextInput
                                style = {{fontSize: 16}}
                                placeholder = { '10 mins before' }
                                placeholderTextColor = {'#C0C0C0'}
                                underlineColorAndroid = {'#C0C0C0'}
                            >
                            </TextInput>
                        </View>

                        <View>
                            <Text style = {{color: '#000000', fontSize: 16}}> Choose color </Text>
                            <View style = {{ height: 50, width: 50, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center' }}>
                                <View style = {{ backgroundColor: '#ff4d4d', borderRadius: 20, height: 40, width: 40}}/>
                            </View>
                        </View>

                    </View>
                    <TouchableHighlight
                        onPress={() => {
                            this.setModalVisible(!this.state.modalVisible);
                        }}
                        style = {styles.buttonAdd}
                    >
                        <View style = {{alignItems: 'center', justifyContent: 'center'}}>
                            <Text style = {{color: '#ffffff'}}> ADD </Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </Modal>
    );
  }}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        marginVertical: 30, 
        marginHorizontal: 20, 
        borderRadius: 5,
        borderColor: 'black',
        borderWidth: 1
    },
    contentInput: {
        flex: 10,
        marginVertical: 20, 
        marginHorizontal: 20, 
        justifyContent: 'space-between', 
        alignItems: 'stretch'
    },
    buttonAdd: {
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: '#66a3ff', 
        borderBottomEndRadius: 3,
        borderBottomStartRadius: 3,
    }
})