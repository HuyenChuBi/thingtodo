import React from 'react';
import {Modal, Text, TouchableHighlight, View, Image, StyleSheet} from 'react-native';

const color = require('../App');
import { height, width } from '../App'

export class EventPopUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
        };
    }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
            <Modal
                animationType = "fade"
                transparent= { true }
                visible={this.state.modalVisible}
            >
                <View style = {styles.container}>
                    <View style = { styles.contentInfor }>
                        <View style = {{alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row'}} >
                            <View style = {{ backgroundColor: 'orange', borderRadius: 10, height: 20, width: 20}}/>
                            <Text style = {{fontSize: 25, marginHorizontal: 10, color: '#000000'}}> Meet Lorence </Text>
                            <View style = {{ height: 0.5, borderWidth: 0.5, width: 70, borderColor: color.gray}}/>
                        </View>
                        <View style = {{alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row', height: 30}} >
                            <View style = {{ height: 50, width: 50, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source= {require('../image/day_view/Bitmap.png')}/>
                            </View>
                            <Text style = {{ marginLeft: 15, fontSize: 16, color: '#000000'}}> 3rd Feb 	• 6 pm </Text>
                        </View>
                        <View style = {{justifyContent: 'flex-start', flexWrap: 'wrap'}}>
                            <Text style = {{fontSize: 16, color: '#000000'}}>Property sets how the browser distributes space between and around content items along the cross-axis of a flexbox container.
                            </Text>
                        </View>
                        <View style = {{alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row'}} >
                            <Image source= {require('../image/day_view/ok.png')} style = {{height: 25, width: 25}}/>
                            <Text style = {{fontSize: 16, color: '#000000', marginLeft: 10}}>Completed </Text>
                        </View>
                    </View>
                    <TouchableHighlight
                        onPress={() => {
                            this.setModalVisible(!this.state.modalVisible);
                        }}
                        style = {styles.buttonDelete}
                    >
                        <View style = {{alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}} >
                            <Image source= {require('../image/day_view/ok.png')}/>
                            <Text style = {{color: '#ffffff', fontSize: 16, marginLeft: 10}}> Delete </Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </Modal>
    );
  }}

const styles = StyleSheet.create({
    container: {
        height: 300,
        marginTop: 30, 
        marginHorizontal: 20, 
        backgroundColor: '#fafafa',
        borderRadius: 5,
        borderColor: 'black',
        borderWidth: 1
    },
    contentInfor: {
        flex: 5, 
        margin: 20, 
        justifyContent: 'space-between', 
        alignItems: 'flex-start'
    },
    buttonDelete: {
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center',
        backgroundColor: '#ff4d4d', 
        borderBottomEndRadius: 3,
        borderBottomStartRadius: 3,
    }
})