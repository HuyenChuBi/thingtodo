import React, { Component } from 'react';
import {  Dimensions } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Onboard from './screens/Onboard';
import Monthly_view from "./screens/Monthly_view"
import DayView from './components/DayView';
import { CardTask } from './components/CardTask';
import { EventPopUp } from './components/EventPopup';
import { CreateEvent } from './components/CreateEvent';


// const ThingToDo = createStackNavigator(
//   {
//     Home: { screen: Onboard },
//     Monthly_view: { screen: Monthly_view },

//   },
//   {
//     headerMode: 'none',
//   },
// );

const ThingToDo = () => <Main/>

export default ThingToDo;

export const { height, width } = Dimensions.get('window');
module.export = {
    color: {
        red: '#ff4d4d',
        blue: '#66a3ff',
        white: '#fafafa',
        gray: '#f2f2f2',
    }
}


