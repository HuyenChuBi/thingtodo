import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, Image, Dimensions, ScrollView, TouchableOpacity, ImageBackground, TextInput } from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import SwitchButton from 'switch-button-react-native';
import TabNavigator from 'react-native-tab-navigator';
import Button from 'react-native-button';
import Modal from 'react-native-modalbox';
import Slider from 'react-native-slider';
import { createStackNavigator, } from 'react-navigation';

var { height, width } = Dimensions.get('window');
var screen = Dimensions.get('window');



export default class Monthly_view extends Component {


    constructor() {

        super();

        this.state = {
            activeSwitch: 1,
            selectedStartDate: null,
            isModalVisible: false,
            isOpen: false,
            isDisabled: false,
            swipeToClose: true,
            sliderValue: 0.3,

        }
    };

    render() {


        const { selectedStartDate } = this.state;
        const startDate = selectedStartDate ? selectedStartDate.toString() : '';


        return (

            <View style={styles.container}>

                <View style={{ alignItems: "center", marginTop: 30 }}>
                    <Image source={require('../image/TTD.png')} style={{ width: 100, height: 30, }} />
                </View>

                <View style={{ margin: 25, alignItems: 'center' }}>
                    <SwitchButton
                        onValueChange={(val) => this.setState({ activeSwitch: val })}      // this is necessary for this component
                        text1='Monthly'                        // optional: first text in switch button --- default ON
                        text2='Daily'                       // optional: second text in switch button --- default OFF
                        switchWidth={300}                 // optional: switch width --- default 44
                        switchHeight={60}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={100}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d4d4d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#ffffff'      // optional: switch background color --- default #fff
                        btnBorderColor='#5a95ff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#5a95ff'      // optional: button background color --- default #00bcd4
                        fontColor='#000000'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#ffffff'            // optional: active font color --- default #fff
                    />

                    {this.state.activeSwitch === 1 ? console.log('view1') : console.log('view2')}

                </View>

                <View style={{ height: 300 }}>
                    <CalendarList
                        // Callback which gets executed when visible months change in scroll view. Default = undefined
                        onVisibleMonthsChange={(months) => { console.log('now these months are visible', months); }}
                        // Max amount of months allowed to scroll to the past. Default = 50
                        pastScrollRange={50}
                        // Max amount of months allowed to scroll to the future. Default = 50
                        futureScrollRange={50}
                        // Enable or disable scrolling of calendar list
                        scrollEnabled={true}
                        // Enable or disable vertical scroll indicator. Default = false
                        showScrollIndicator={true}
                        markedDates={onDateChange = this.onDateChange}
                        onDayPress={(day) => this.refs.modal3.open()}
                    //     '2018-10-26': { selected: true, marked: true, selectedColor: '#5a95ff' },
                    //     '2018-10-26': { marked: true },
                    //     '2018-10-27': { marked: true, dotColor: 'red', activeOpacity: 0 },
                    //     '2018-10-29': { disabled: true, disableTouchEvent: true }
                    // }} 

                    />

                </View>
                <View style={styles.wrapper}>

                </View>

                <Modal
                    style={[styles.modal, styles.modal3]}
                    position={"center"}
                    ref={"modal3"}
                    isSave={this.state.isSave}
                    isCancel={this.state.isCancel}


                >
                    <TextInput style={styles.text}></TextInput>
                    <Button onPress={() => this.setState({ isSave: !this.state.isSave })} style={styles.btn}>Save</Button>
                </Modal>

                <TabNavigator>

                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'List'}
                        renderIcon={() => <Image source={require('../image/List.png')} style={{ width: 25, height: 17 }} />}
                        renderSelectedIcon={() => <Image source={require('../image/List.png')} style={{ width: 25, height: 17 }} />}
                        // badgeText="1"
                        onPress={() => this._deleteHotspot()}>

                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'Clock'}
                        renderIcon={() => <Image source={require('../image/clock.png')} style={{ width: 23, height: 23 }} />}
                        renderSelectedIcon={() => <Image source={require('../image/clock.png')} style={{ width: 23, height: 23 }} />}
                        onPress={() => () => this._deleteHotspot()}>
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'Bell'}
                        renderIcon={() => <Image source={require('../image/Bell.png')} style={{ width: 23, height: 26 }} />}
                        renderSelectedIcon={() => <Image source={require('../image/Bell.png')} style={{ width: 23, height: 26 }} />}
                        onPress={() => () => this._deleteHotspot()}>
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'User'}
                        renderIcon={() => <Image source={require('../image/User.png')} style={{ width: 23, height: 24 }} />}
                        renderSelectedIcon={() => <Image source={require('../image/User.png')} style={{ width: 23, height: 24 }} />}
                        onPress={() => () => this._deleteHotspot()}>
                    </TabNavigator.Item>



                </TabNavigator>

                <View style={{ position: 'absolute', marginVertical: 527 }}>
                    <TouchableOpacity onPress={this._onPressButton}>
                        <Image
                            style={{ marginHorizontal: 150, alignItems: 'flex-end' }}
                            source={require('../image/add.png')}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ backgroundColor: "#f8fbfc" }}>
                    <FlatList

                        data={this.state.listData}
                        renderItem={({ item }) => <Text style={styles.item}>{item.key}</Text>}
                    />


                </View>





            </View >


        );
    }


}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#F5FCFF',
        flexDirection: 'column',
        backgroundColor: "#ffffff"
    },
    atop: {
        flex: 1,

    },
    btop: {
        width: width,
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',

    },
    button: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',


    },
    // switchbutton: {

    // },
    // imput: {
    //     height: 40,
    //     paddingHorizontal: 20,
    //     width: 300,
    //     margin: 20,
    //     alignItems: "center"

    //     // backgroundColor: '#ffffff'
    // },
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17
    },
    emptyDate: {
        height: 15,
        flex: 1,
        paddingTop: 30
    },
    wrapper: {
        paddingTop: 50,
        flex: 1
    },

    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 300
    },

    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },

    text: {
        color: "black",
        fontSize: 22
    },
    textinput: {
        backgroundColor: "#ffffff",
        width: 250,
        height: 50,
        borderColor: "#ffffff",


    }



});
