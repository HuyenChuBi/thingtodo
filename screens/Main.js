import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground, StatusBar} from 'react-native';
import SwitchButton from 'switch-button-react-native';
import TabNavigator from 'react-native-tab-navigator';

import  { DayView } from '../components/DayView';
import { CreateEvent } from '../components/CreateEvent';

export default class Container extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeSwitch: true,
            visiableAddModal: false,

        }
    };

    render() {
        return (
            <ImageBackground source={require('../image/background.png')} style={{ width: '100%', height: '100%' }}>
                <View style={styles.container}>
                    <StatusBar hidden = { true }/>
                    <View style={{ alignItems: "center"}}>
                        <Image source={require('../image/TTD.png')} style={{ width: 80, height: 25, marginTop: 30 }} />
                        <SwitchButton
                            onValueChange={(val) => this.setState({ activeSwitch: val })}      // this is necessary for this component
                            text1='Monthly'                        // optional: first text in switch button --- default ON
                            text2='Daily'                       // optional: second text in switch button --- default OFF
                            switchWidth={300}                 // optional: switch width --- default 44
                            switchHeight={50}                 // optional: switch height --- default 100
                            switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                            switchBorderRadius={80}          // optional: switch border radius --- default oval
                            switchSpeedChange={500}           // optional: button change speed --- default 100
                            switchBorderColor ='#d4d4d4'       // optional: switch border color --- default #d4d4d4
                            switchBackgroundColor='#ffffff'      // optional: switch background color --- default #fff
                            btnBorderColor='#5a95ff'          // optional: button border color --- default #00a4b9
                            btnBackgroundColor='#5a95ff'      // optional: button background color --- default #00bcd4
                            fontColor='#000000'               // optional: text font color --- default #b1b1b1
                            activeFontColor='#ffffff'            // optional: active font color --- default #fff
                        />
                    </View>

                    { this.state.activeSwitch && <View style = {{ height: 300, backgroundColor: '#5a95ff'}}/> }
                    { this.state.activeSwitch && <DayView/>}

                    <TabNavigator>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'List'}
                            renderIcon={() => <Image source={require('../image/List.png')} style={{ width: 25, height: 17 }} />}
                            renderSelectedIcon={() => <Image source={require('../image/List.png')} style={{ width: 25, height: 17 }} />}
                            onPress={() => this._deleteHotspot()}>
                        </TabNavigator.Item>

                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Clock'}
                            renderIcon={() => <Image source={require('../image/clock.png')} style={{ width: 23, height: 23 }} />}
                            renderSelectedIcon={() => <Image source={require('../image/clock.png')} style={{ width: 23, height: 23 }} />}
                            onPress={() => () => this._deleteHotspot()}>
                        </TabNavigator.Item>

                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'Bell'}
                            renderIcon={() => <Image source={require('../image/Bell.png')} style={{ width: 23, height: 26 }} />}
                            renderSelectedIcon={() => <Image source={require('../image/Bell.png')} style={{ width: 23, height: 26 }} />}
                            onPress={() => () => this._deleteHotspot()}>
                        </TabNavigator.Item>

                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'User'}
                            renderIcon={() => <Image source={require('../image/User.png')} style={{ width: 23, height: 24 }} />}
                            renderSelectedIcon={() => <Image source={require('../image/User.png')} style={{ width: 23, height: 24 }} />}
                            onPress={() => () => this._deleteHotspot()}>
                        </TabNavigator.Item>
                    </TabNavigator>

                    <View style={{ position: 'absolute', marginBottom: 15 }}>
                        <TouchableOpacity onPress={this._onPressAdd}>
                            <Image
                                style={{ marginHorizontal: 150, alignItems: 'flex-end' }}
                                source={require('../image/add.png')}
                            />
                        </TouchableOpacity>
                        { this.state.visiableAddModal && <CreateEvent hideAddModal = { this.hideAddModal }/>}
                    </View>
                    {/* <View style={{ backgroundColor: "#f8fbfc" }}>
                        <FlatList
                            data={this.state.listData}
                            renderItem={({ item }) => <Text style={styles.item}>{item.key}</Text>}
                        />
                    </View> */}
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-betwwen',
    },
});
